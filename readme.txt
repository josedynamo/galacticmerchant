Assumptions

The system will read some instructions from a text file. I have classified these instructions in 2 groups.

Commands and Queries.

The commands are statements used to configure the objects that will be needed to answer the queries requested later.

Examples of commands are:

- glob is I
- glob glob Silver is 34 Credits

The queries use the objects configured by the commands to provide an answer.

Examples of queries are:

how much is pish tegj glob glob ?
how many Credits is glob prok Iron ?

To validate these instructions I use regular expressions for each different type of query. In case the instruction
doesn't match any of the queries, the program will return "I have no idea what you are talking about".

In the case the user performs a valid query but the objects needed to answer it have not been
configured yet, the system will also return "I have no idea what you are talking about" but will continue parsing the
next line until it's done with all of them.

For example:

prok is V
glob is I
glob glob Silver is 34 Credits
how many Credits is drak prok Silver ? <- will return "I have no idea what you are talking about" as drak is not defined.
drak is C
how many Credits is drak prok Silver ? <- will return "drak prok Silver is 1785 Credits" as drak is now defined



BUILD AND EXECUTION

Gradle Setup Instructions

The project was built using Gradle and this is the easiest way to build it from scratch and run it. I couldn't provide
the wrapper that would allow standalone execution of Gradle as this is a jar file and you clearly specify not to
include executables in the zip file.

There are several ways to get Gradle. For more details, you can go here https://gradle.org/install/#install.

For Mac Users.

If you're on a Mac and have "homebrew" installed, you get can the latest version of Gradle by running `brew install gradle`

Build instructions

1. Once you have installed Gradle, unzip the project file and go to the "galacticmerchant" folder.
2. Run "gradle assemble"

Running the program

1. Go to the folder build/libs.
2. You should see a jar called merchant-1.0-SNAPSHOT.jar there.
3. Copy the file you want to use as input for the program to this folder. (There's an example in /src/main/resources/testdata.txt)
4. Run java -jar merchant-1.0-SNAPSHOT.jar <filename>
5. You should see the output of the program on your screen.

