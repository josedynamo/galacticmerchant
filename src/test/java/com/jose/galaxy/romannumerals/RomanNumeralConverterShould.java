package com.jose.galaxy.romannumerals;

import com.jose.galaxy.conversions.GalacticNumeralConversion;
import com.jose.galaxy.exception.GalacticNumeralException;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

import java.util.AbstractMap;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

public class RomanNumeralConverterShould {

    RomanNumeralConverter romanNumeralConverter = new RomanNumeralConverter();
    private GalacticNumeralConversion galacticNumeralConversion;

    @Before
    public void setup() {
        galacticNumeralConversion = new GalacticNumeralConversion();
        galacticNumeralConversion.addConversion(new AbstractMap.SimpleEntry<>("glop", "I"));
        galacticNumeralConversion.addConversion(new AbstractMap.SimpleEntry<>("prok", "IV"));
        galacticNumeralConversion.addConversion(new AbstractMap.SimpleEntry<>("tegj", "V"));

    }

    @Test
    public void validate_for_correct_roman_numerals() throws GalacticNumeralException {
        assertTrue(romanNumeralConverter.isValidNumeral("I"));
        assertTrue(romanNumeralConverter.isValidNumeral("II"));
        assertTrue(romanNumeralConverter.isValidNumeral("III"));
        assertTrue(romanNumeralConverter.isValidNumeral("IV"));
        assertTrue(romanNumeralConverter.isValidNumeral("V"));
        assertTrue(romanNumeralConverter.isValidNumeral("VI"));
        assertTrue(romanNumeralConverter.isValidNumeral("VII"));
        assertTrue(romanNumeralConverter.isValidNumeral("VIII"));
        assertTrue(romanNumeralConverter.isValidNumeral("IX"));
        assertTrue(romanNumeralConverter.isValidNumeral("X"));
        assertTrue(romanNumeralConverter.isValidNumeral("XIII"));
        assertTrue(romanNumeralConverter.isValidNumeral("XIX"));
        assertTrue(romanNumeralConverter.isValidNumeral("XXV"));
        assertTrue(romanNumeralConverter.isValidNumeral("XXXIX"));
        assertTrue(romanNumeralConverter.isValidNumeral("XLV"));
        assertTrue(romanNumeralConverter.isValidNumeral("LV"));
        assertTrue(romanNumeralConverter.isValidNumeral("LIX"));
        assertTrue(romanNumeralConverter.isValidNumeral("LXXVIII"));
        assertTrue(romanNumeralConverter.isValidNumeral("LXXXIX"));
        assertTrue(romanNumeralConverter.isValidNumeral("XC"));
        assertTrue(romanNumeralConverter.isValidNumeral("XCV"));
        assertTrue(romanNumeralConverter.isValidNumeral("XCIX"));
        assertTrue(romanNumeralConverter.isValidNumeral("C"));
        assertTrue(romanNumeralConverter.isValidNumeral("CI"));
        assertTrue(romanNumeralConverter.isValidNumeral("CII"));
        assertTrue(romanNumeralConverter.isValidNumeral("CX"));
        assertTrue(romanNumeralConverter.isValidNumeral("CXII"));
        assertTrue(romanNumeralConverter.isValidNumeral("CXIII"));
        assertTrue(romanNumeralConverter.isValidNumeral("CXV"));
        assertTrue(romanNumeralConverter.isValidNumeral("CXVIII"));
        assertTrue(romanNumeralConverter.isValidNumeral("CXIX"));
        assertTrue(romanNumeralConverter.isValidNumeral("CXXXVIII"));
        assertTrue(romanNumeralConverter.isValidNumeral("CLVIII"));
        assertTrue(romanNumeralConverter.isValidNumeral("CD"));
        assertTrue(romanNumeralConverter.isValidNumeral("CDXC"));
        assertTrue(romanNumeralConverter.isValidNumeral("CDXCIX"));
        assertTrue(romanNumeralConverter.isValidNumeral("DXCIX"));
        assertTrue(romanNumeralConverter.isValidNumeral("CMXCIX"));
        assertTrue(romanNumeralConverter.isValidNumeral("MI"));
        assertTrue(romanNumeralConverter.isValidNumeral("MDXCIX"));
        assertTrue(romanNumeralConverter.isValidNumeral("MDXXI"));
        assertTrue(romanNumeralConverter.isValidNumeral("MMDXXI"));
        assertTrue(romanNumeralConverter.isValidNumeral("MCMXCII"));
        assertTrue(romanNumeralConverter.isValidNumeral("MCMXCIII"));
        assertTrue(romanNumeralConverter.isValidNumeral("MMMMCMXCIX"));


    }

    @Test
    public void convert_galactic_number_to_RomanNumeral() throws GalacticNumeralException {
        assertThat(romanNumeralConverter.getRomanFromGalacticNumber("glop", galacticNumeralConversion), is("I"));
        assertThat(romanNumeralConverter.getRomanFromGalacticNumber("glop glop", galacticNumeralConversion), is("II"));
        assertThat(romanNumeralConverter.getRomanFromGalacticNumber("glop glop glop", galacticNumeralConversion), is("III"));
        assertThat(romanNumeralConverter.getRomanFromGalacticNumber("prok", galacticNumeralConversion), is("IV"));
        assertThat(romanNumeralConverter.getRomanFromGalacticNumber("tegj glop glop", galacticNumeralConversion), is("VII"));

    }

    @Test
    public void convert_roman_numeral_to_arabic() throws GalacticNumeralException {
        assertThat(romanNumeralConverter.toArabic("I"), is(1));
        assertThat(romanNumeralConverter.toArabic("II"), is(2));
        assertThat(romanNumeralConverter.toArabic("III"), is(3));
        assertThat(romanNumeralConverter.toArabic("IV"), is(4));
        assertThat(romanNumeralConverter.toArabic("V"), is(5));
        assertThat(romanNumeralConverter.toArabic("VI"), is(6));
        assertThat(romanNumeralConverter.toArabic("VII"), is(7));
        assertThat(romanNumeralConverter.toArabic("VIII"), is(8));
        assertThat(romanNumeralConverter.toArabic("IX"), is(9));
        assertThat(romanNumeralConverter.toArabic("X"), is(10));
        assertThat(romanNumeralConverter.toArabic("XII"), is(12));
        assertThat(romanNumeralConverter.toArabic("XIII"), is(13));
        assertThat(romanNumeralConverter.toArabic("XIV"), is(14));
        assertThat(romanNumeralConverter.toArabic("XV"), is(15));
        assertThat(romanNumeralConverter.toArabic("XVII"), is(17));
        assertThat(romanNumeralConverter.toArabic("XIX"), is(19));
        assertThat(romanNumeralConverter.toArabic("XXXV"), is(35));
        assertThat(romanNumeralConverter.toArabic("XLI"), is(41));
        assertThat(romanNumeralConverter.toArabic("XLII"), is(42));
        assertThat(romanNumeralConverter.toArabic("XLVIII"), is(48));
        assertThat(romanNumeralConverter.toArabic("LVI"), Is.is(56));
        assertThat(romanNumeralConverter.toArabic("LIX"), Is.is(59));
        assertThat(romanNumeralConverter.toArabic("LXXV"), Is.is(75));
        assertThat(romanNumeralConverter.toArabic("XC"), Is.is(90));
        assertThat(romanNumeralConverter.toArabic("XCV"), Is.is(95));
        assertThat(romanNumeralConverter.toArabic("C"), Is.is(100));
        assertThat(romanNumeralConverter.toArabic("CL"), Is.is(150));
        assertThat(romanNumeralConverter.toArabic("CCLXII"), Is.is(262));
        assertThat(romanNumeralConverter.toArabic("CCCXC"), Is.is(390));
        assertThat(romanNumeralConverter.toArabic("CCCXCIX"), Is.is(399));
        assertThat(romanNumeralConverter.toArabic("CD"), Is.is(400));
        assertThat(romanNumeralConverter.toArabic("DL"), Is.is(550));
        assertThat(romanNumeralConverter.toArabic("M"), Is.is(1000));
        assertThat(romanNumeralConverter.toArabic("MMM"), Is.is(3000));
        assertThat(romanNumeralConverter.toArabic("MV"), Is.is(1005));
        assertThat(romanNumeralConverter.toArabic("MD"), Is.is(1500));
        assertThat(romanNumeralConverter.toArabic("MCMIII"), Is.is(1903));
        assertThat(romanNumeralConverter.toArabic("MCMLXXXI"), Is.is(1981));
        assertThat(romanNumeralConverter.toArabic("MMDXXIX"), Is.is(2529));
        assertThat(romanNumeralConverter.toArabic("MMCM"), Is.is(2900));
        assertThat(romanNumeralConverter.toArabic("MMCL"), Is.is(2150));
        assertThat(romanNumeralConverter.toArabic("MMM"), Is.is(3000));
        assertThat(romanNumeralConverter.toArabic("MMMMCMXCIX"), Is.is(4999));

    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_IIV() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("IIV");

    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_VIIII() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("VIIII");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_IIX() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("IIX");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_XXXX() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("XXXX");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_XIXI() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("XIXI");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_XVV() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("XVV");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_XXL() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("XXL");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_XLL() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("XLL");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_LXLVIII() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("LXLVIII");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_XCC() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("XCC");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_XCLC() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("XCLC");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_LIXX() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("LIXX");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_CCCC() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("CCCC");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_CDIIIX() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("CDIIIX");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_CMCD() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("CMCD");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_CMIIII() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("CMIIII");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_MMDXXIXX() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("MMDXXIXX");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_MMMCMLD() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("MMMCMLD");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_MMMMCMIVL() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("MMMMCMIVL");
    }

    @Test(expected = GalacticNumeralException.class)
    public void throw_galactic_numeral_exception_when_roman_numeral_is_MMMMM() throws GalacticNumeralException {
        romanNumeralConverter.isValidNumeral("MMMMM");
    }




}