package com.jose.galaxy.parser.impl;

import com.jose.galaxy.conversions.GalacticElementConversion;
import com.jose.galaxy.conversions.GalacticNumeralConversion;
import com.jose.galaxy.exception.GalacticNumeralException;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class NumeralConversionGalacticParserShould {

    private NumeralConversionGalacticParser numeralConversionGalacticParser;
    private GalacticNumeralConversion galacticNumeralConversion;
    private GalacticElementConversion galacticElementConversion;

    @Before
    public void setUp() {
        numeralConversionGalacticParser = new NumeralConversionGalacticParser();
        galacticNumeralConversion = new GalacticNumeralConversion();
        galacticElementConversion = new GalacticElementConversion();

    }

    @Test
    public void parse_conversion_instruction_and_add_conversion() throws Exception {
        numeralConversionGalacticParser.parse("glob is I", galacticNumeralConversion, galacticElementConversion);
        numeralConversionGalacticParser.parse("prok is V", galacticNumeralConversion, galacticElementConversion);
        assertThat(galacticNumeralConversion.getConversion("glob"), is("I"));
        assertThat(galacticNumeralConversion.getConversion("prok"), is("V"));

    }

    @Test
    public void parse_conversion_instruction_and_replace_conversion() throws GalacticNumeralException {
        numeralConversionGalacticParser.parse("glob is I", galacticNumeralConversion, galacticElementConversion);
        numeralConversionGalacticParser.parse("prok is V", galacticNumeralConversion, galacticElementConversion);
        numeralConversionGalacticParser.parse("prok is XC", galacticNumeralConversion, galacticElementConversion);
        numeralConversionGalacticParser.parse("glob is M", galacticNumeralConversion, galacticElementConversion);
        assertThat(galacticNumeralConversion.getConversion("glob"), is("M"));
        assertThat(galacticNumeralConversion.getConversion("prok"), is("XC"));


    }

}