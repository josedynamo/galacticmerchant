package com.jose.galaxy.parser.impl;

import com.jose.galaxy.conversions.GalacticElementConversion;
import com.jose.galaxy.conversions.GalacticNumeralConversion;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class CreditsGalacticParserShould {


    private GalacticNumeralConversion galacticNumeralConversion;
    private GalacticElementConversion galacticElementConversion;
    private ElementValueGalacticParser elementValueGalacticParser = new ElementValueGalacticParser();
    private NumeralConversionGalacticParser numeralConversionGalacticParser;
    private CreditsGalacticParser galacticCreditsOracle = new CreditsGalacticParser();

    @Before
    public void init() {
        galacticNumeralConversion = new GalacticNumeralConversion();
        galacticElementConversion = new GalacticElementConversion();
        elementValueGalacticParser = new ElementValueGalacticParser();
        numeralConversionGalacticParser = new NumeralConversionGalacticParser();
        numeralConversionGalacticParser.parse("glob is I", galacticNumeralConversion, null);
        numeralConversionGalacticParser.parse("prok is V", galacticNumeralConversion, null);
        numeralConversionGalacticParser.parse("pish is X", galacticNumeralConversion, null);
        numeralConversionGalacticParser.parse("tegl is L", galacticNumeralConversion, null);
        numeralConversionGalacticParser.parse("ark is C", galacticNumeralConversion, null);
        elementValueGalacticParser.parse("glob glob Silver is 34 Credits", galacticNumeralConversion, galacticElementConversion);
        elementValueGalacticParser.parse("glob prok Gold is 57800 Credits", galacticNumeralConversion, galacticElementConversion);
        elementValueGalacticParser.parse("pish pish Iron is 3910 Credits", galacticNumeralConversion, galacticElementConversion);
        elementValueGalacticParser.parse("ark ark Carbon is 1000 Credits", galacticNumeralConversion, galacticElementConversion);

    }

    @Test
    public void return_68_credits_for_ark_ark_Uranium() {
        assertThat(galacticCreditsOracle.parse("how many Credits is tegl Carbon ?"
                , galacticNumeralConversion, galacticElementConversion), is("tegl Carbon is 250 Credits"));

    }

    @Test
    public void return_5_credits_for_glob_prok_Silver() {
        assertThat(galacticCreditsOracle.parse("how many Credits is glob prok Silver ?"
                , galacticNumeralConversion, galacticElementConversion), is("glob prok Silver is 68 Credits"));

    }

    @Test
    public void return_57800_credits_for_glob_prok_Gold() {
        assertThat(galacticCreditsOracle.parse("how many Credits is glob prok Gold ?"
                , galacticNumeralConversion, galacticElementConversion), is("glob prok Gold is 57800 Credits"));

    }

    @Test
    public void return_782_credits_for_glob_prok_Iron() {
        assertThat(galacticCreditsOracle.parse("how many Credits is glob prok Iron ?"
                , galacticNumeralConversion, galacticElementConversion), is("glob prok Iron is 782 Credits"));

    }
}
