package com.jose.galaxy.parser.impl;

import com.jose.galaxy.conversions.GalacticNumeralConversion;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SimpleQuestionGalacticParserShould {

    private NumeralConversionGalacticParser numeralConversionGalacticParser = new NumeralConversionGalacticParser();
    private GalacticNumeralConversion galacticNumeralToRomanConversion = new GalacticNumeralConversion();
    private SimpleQuestionGalacticParser simpleQuestionGalacticParser = new SimpleQuestionGalacticParser();


    @Before
    public void setup() {
        numeralConversionGalacticParser.parse("glob is I", galacticNumeralToRomanConversion, null);
        numeralConversionGalacticParser.parse("prok is V", galacticNumeralToRomanConversion, null);
        numeralConversionGalacticParser.parse("pish is X", galacticNumeralToRomanConversion, null);
        numeralConversionGalacticParser.parse("tegj is L", galacticNumeralToRomanConversion, null);
    }

    @Test
    public void return_42_for_pish_tegj_glob_glob() {
        assertThat(simpleQuestionGalacticParser.parse("how much is pish tegj glob glob ?"
                , galacticNumeralToRomanConversion, null), is("pish tegj glob glob is 42"));

    }

    @Test
    public void return_25_for_pish_tegj_glob_glob() {
        assertThat(simpleQuestionGalacticParser.parse("how much is pish pish prok ?"
                , galacticNumeralToRomanConversion, null), is("pish pish prok is 25"));

    }

    @Test
    public void return_50_for_pish_tegj_glob_glob() {
        assertThat(simpleQuestionGalacticParser.parse("how much is tegj ?"
                , galacticNumeralToRomanConversion, null), is("tegj is 50"));

    }
}
