package com.jose.galaxy.parser.impl;

import com.jose.galaxy.conversions.GalacticElementConversion;
import com.jose.galaxy.conversions.GalacticNumeralConversion;
import com.jose.galaxy.exception.GalacticElementNotFoundException;
import com.jose.galaxy.exception.GalacticNumeralException;

import org.junit.Before;
import org.junit.Test;

import java.util.AbstractMap;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ElementValueGalacticParserShould {

    private ElementValueGalacticParser elementValueGalacticParser;
    private GalacticNumeralConversion galacticNumeralConversion;
    private GalacticElementConversion galacticElementConversion;

    @Before
    public void setUp() {
        elementValueGalacticParser = new ElementValueGalacticParser();
        galacticElementConversion = new GalacticElementConversion();
        galacticNumeralConversion = new GalacticNumeralConversion();
    }


    @Test(expected = GalacticElementNotFoundException.class)
    public void throw_galactic_element_not_found_exception_if_roman_numeral_mapping_not_defined() throws GalacticElementNotFoundException {
        elementValueGalacticParser.parse("glob glob Silver is 34 Credits", galacticNumeralConversion, galacticElementConversion);
        assertThat(galacticElementConversion.getElementValueConversion("Silver"), is(17.0f));

    }

    @Test
    public void parse_conversion_instruction_and_add_conversion() throws GalacticElementNotFoundException {
        galacticNumeralConversion.addConversion(new AbstractMap.SimpleEntry<>("glob", "I"));
        galacticNumeralConversion.addConversion(new AbstractMap.SimpleEntry<>("prok", "V"));
        elementValueGalacticParser.parse("glob glob Silver is 34 Credits", galacticNumeralConversion, galacticElementConversion);
        elementValueGalacticParser.parse("glob prok Gold is 57800 Credits", galacticNumeralConversion, galacticElementConversion);
        assertThat(galacticElementConversion.getElementValueConversion("Silver"), is(17.0f));
        assertThat(galacticElementConversion.getElementValueConversion("Gold"), is(14450.0f));

    }

}
