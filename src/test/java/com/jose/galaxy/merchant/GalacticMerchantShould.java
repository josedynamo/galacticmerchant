package com.jose.galaxy.merchant;

import com.jose.galaxy.parser.GalacticParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


/**
 * Acceptance Test for GalacticMerchant
 * <p>
 * Created by jose on 18/07/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class GalacticMerchantShould {

    private GalacticMerchant galacticMerchant;

    @Before
    public void setup() {
        galacticMerchant = new GalacticMerchant();
    }

    @Test
    public void return_empty_string_when_input_is_glob_is_I() {
        assertThat(galacticMerchant.readInstruction("glop is I"), is(""));

    }

    @Test
    public void return_empty_string_when_input_is_glop_glop_Silver_is_34_credits() {
        assertThat(galacticMerchant.readInstruction("glop is I"), is(""));
        assertThat(galacticMerchant.readInstruction("glop glop Silver is 34 Credits"), is(""));

    }

    @Test
    public void return_pish_tegj_glob_is_42_when_asked_how_much_is_pish_tegj_glob_glob_() {
        galacticMerchant.readInstruction("glop is I");
        galacticMerchant.readInstruction("prok is V");
        galacticMerchant.readInstruction("pish is X");
        galacticMerchant.readInstruction("tegj is L");
        assertThat(galacticMerchant.readInstruction("how much is pish tegj glop glop ?"), is("pish tegj glop glop is 42"));
    }

    @Test
    public void return_glob_prok_Silver_is_68_Credits_when_asked_how_many_Credits_is_glob_prok_Silver() {
        galacticMerchant.readInstruction("glop is I");
        galacticMerchant.readInstruction("prok is V");
        galacticMerchant.readInstruction("glop glop Silver is 34 Credits");
        assertThat(galacticMerchant.readInstruction("how many Credits is glop prok Silver ?"), is("glop prok Silver is 68 Credits"));
    }

    @Test
    public void return_I_have_no_idea_answer_when_asked_how_much_wood_question_() {
        assertThat(galacticMerchant.readInstruction("how much wood could a woodchuck chuck if a woodchuck could chuck wood ?"), is(GalacticParser.I_DON_T_KNOW_WHAT_YOU_RE_TALKING_ABOUT));
    }
}
