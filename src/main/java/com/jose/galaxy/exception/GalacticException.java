package com.jose.galaxy.exception;

public class GalacticException extends Exception {

    public GalacticException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return " " + getMessage();
    }
}
