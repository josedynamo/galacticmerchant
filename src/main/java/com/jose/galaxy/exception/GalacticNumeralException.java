package com.jose.galaxy.exception;

public class GalacticNumeralException extends GalacticException {
    public GalacticNumeralException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return " " + getMessage();
    }
}
