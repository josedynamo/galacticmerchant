package com.jose.galaxy.exception;

public class GalacticElementNotFoundException extends GalacticException {
    public GalacticElementNotFoundException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return " " + getMessage();
    }
}
