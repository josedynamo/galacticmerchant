package com.jose.galaxy.parser;

import com.jose.galaxy.conversions.GalacticElementConversion;
import com.jose.galaxy.conversions.GalacticNumeralConversion;

/**
 * Parses an instruction and returns an answer or empty string in case no answer is needed.
 */
public interface GalacticParser {

    String I_DON_T_KNOW_WHAT_YOU_RE_TALKING_ABOUT = "I don't know what you're talking about";

    String parse(String instruction, GalacticNumeralConversion galacticNumeralConversion, GalacticElementConversion galacticElementConversion);
}
