package com.jose.galaxy.parser.impl;

import com.jose.galaxy.conversions.GalacticElementConversion;
import com.jose.galaxy.conversions.GalacticNumeralConversion;
import com.jose.galaxy.parser.GalacticParser;

import java.util.AbstractMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * /**
 * Parses instructions like:
 * <p>
 * glop is V
 * <p>
 * and saves them in a GalacticNumeralConversion object.
 */
public class NumeralConversionGalacticParser implements GalacticParser {

    public static final String REG_EX = "(?<galacticNumeral>\\w*)\\s(is)\\s*(?<romanNumeral>M|CM|D|CD|C|XC|L|XL|X|IX|V|IV|I)";

    @Override
    public String parse(String galacticInstruction, GalacticNumeralConversion galacticNumeralConversion, GalacticElementConversion galacticElementConversion) {
        if (galacticInstruction.matches(REG_EX)) {
            addConversionEntry(galacticInstruction, galacticNumeralConversion, REG_EX);
        }
        return "";
    }

    private void addConversionEntry(String galacticInstruction, GalacticNumeralConversion galacticNumeralConversion, String conversionDefinition) {
        Pattern pattern = Pattern.compile(conversionDefinition);
        final Matcher matcher = pattern.matcher(galacticInstruction);
        matcher.find();
        Map.Entry<String, String> galacticToRomanConversion = new AbstractMap.SimpleEntry<>(matcher.group("galacticNumeral"), matcher.group("romanNumeral"));
        galacticNumeralConversion.addConversion(galacticToRomanConversion);
    }
}
