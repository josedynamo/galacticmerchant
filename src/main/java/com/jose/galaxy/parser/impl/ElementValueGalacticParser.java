package com.jose.galaxy.parser.impl;

import com.jose.galaxy.conversions.GalacticElementConversion;
import com.jose.galaxy.conversions.GalacticNumeralConversion;
import com.jose.galaxy.exception.GalacticNumeralException;
import com.jose.galaxy.parser.GalacticParser;
import com.jose.galaxy.romannumerals.RomanNumeralConverter;

import java.util.AbstractMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses instructions like:
 * <p>
 * glob prok Gold is 57800 Credits
 * <p>
 * and saves them in a GalacticElementConversion object.
 */
public class ElementValueGalacticParser implements GalacticParser {
    public static final String REG_EX = "(?<galacticNumeral>\\w*\\s)+(?<element>\\w*)\\sis\\s(?<credits>\\d*)\\sCredits";
    private static final String ELEMENT = "element";
    private static final String CREDITS = "credits";
    private RomanNumeralConverter romanNumeralConverter = new RomanNumeralConverter();

    @Override
    public String parse(String instruction, GalacticNumeralConversion galacticNumeralConversion, GalacticElementConversion galacticElementConversion) {
        try {
            if (instruction.matches(REG_EX)) {
                addGalacticElementConversion(instruction, galacticNumeralConversion, galacticElementConversion);
            }
        } catch (GalacticNumeralException e) {
            return GalacticParser.I_DON_T_KNOW_WHAT_YOU_RE_TALKING_ABOUT;
        }
        return "";
    }

    private void addGalacticElementConversion(String instruction, GalacticNumeralConversion galacticNumeralConversion, GalacticElementConversion galacticElementConversion) throws GalacticNumeralException {
        Pattern pattern = Pattern.compile(REG_EX);
        final Matcher matcher = pattern.matcher(instruction);
        matcher.find();
        String element = matcher.group(ELEMENT);
        float credits = Float.parseFloat(matcher.group(CREDITS));
        final String galacticNumber = instruction.split(element)[0];
        final String romanFromGalacticNumber = romanNumeralConverter.getRomanFromGalacticNumber(galacticNumber, galacticNumeralConversion);
        final Integer arabicNumber = romanNumeralConverter.toArabic(romanFromGalacticNumber);
        float value = credits / arabicNumber;
        galacticElementConversion.addElementValueConversion(new AbstractMap.SimpleEntry<>(element, value));
    }
}
