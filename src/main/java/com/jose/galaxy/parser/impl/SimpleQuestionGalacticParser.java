package com.jose.galaxy.parser.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.jose.galaxy.conversions.GalacticElementConversion;
import com.jose.galaxy.conversions.GalacticNumeralConversion;
import com.jose.galaxy.exception.GalacticNumeralException;
import com.jose.galaxy.parser.GalacticParser;
import com.jose.galaxy.romannumerals.RomanNumeralConverter;

/**
 * Parses instructions like:
 * <p>
 * how much is pish tegj glob glob ?
 * <p>
 * and returns answers like:
 * <p>
 * pish tegj glob glob is 42
 */
public class SimpleQuestionGalacticParser implements GalacticParser {

    public static final String REG_EX = "(?<question>how\\smuch\\sis\\s)((?<galacticNumeral>\\w*\\s*))+\\?";
    private static String answer = "%s is %s";
    private RomanNumeralConverter romanNumeralConverter = new RomanNumeralConverter();

    @Override
    public String parse(String instruction, GalacticNumeralConversion galacticNumeralConversion, GalacticElementConversion galacticElementConversion) {
        Pattern pattern = Pattern.compile(REG_EX);
        final Matcher matcher = pattern.matcher(instruction);
        matcher.find();
        final String galacticNumber = instruction.replace(matcher.group("question"), "").replace(" ?", "");
        try {
            final String romanFromGalacticNumber = romanNumeralConverter.getRomanFromGalacticNumber(galacticNumber, galacticNumeralConversion);
            return String.format(answer, galacticNumber, romanNumeralConverter.toArabic(romanFromGalacticNumber));
        } catch (GalacticNumeralException e) {
            return I_DON_T_KNOW_WHAT_YOU_RE_TALKING_ABOUT;
        }


    }
}
