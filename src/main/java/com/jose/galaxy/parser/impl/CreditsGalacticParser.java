package com.jose.galaxy.parser.impl;

import com.jose.galaxy.conversions.GalacticElementConversion;
import com.jose.galaxy.conversions.GalacticNumeralConversion;
import com.jose.galaxy.exception.GalacticException;
import com.jose.galaxy.parser.GalacticParser;
import com.jose.galaxy.romannumerals.RomanNumeralConverter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses Instructions like :
 * <p>
 * how many Credits is glob prok Silver ?
 * <p>
 * and returns:
 * <p>
 * glob prok Silver is 68 Credits
 */
public class CreditsGalacticParser implements GalacticParser {

    public static final String REG_EX = "(?<question>how\\smany\\sCredits\\sis\\s)(?<galacticNumeral>\\w*\\s*)+\\s((?<element>\\w*)\\s\\?)";
    private static String answer = "%s %s is %d Credits";
    private RomanNumeralConverter romanNumeralConverter = new RomanNumeralConverter();

    @Override
    public String parse(String instruction, GalacticNumeralConversion galacticNumeralConversion, GalacticElementConversion galacticElementConversion) {
        try {
            Pattern pattern = Pattern.compile(REG_EX);
            final Matcher matcher = pattern.matcher(instruction);
            matcher.find();
            String element = matcher.group("element");
            final String galacticNumber = instruction.replace(matcher.group("question"), "").replace(" " + element, "").replace(" ?", "");
            final String romanFromGalacticNumber = romanNumeralConverter.getRomanFromGalacticNumber(galacticNumber, galacticNumeralConversion);
            final Integer arabicNumber;

            arabicNumber = romanNumeralConverter.toArabic(romanFromGalacticNumber);
            final Float elementValueConversion = galacticElementConversion.getElementValueConversion(element);
            final float result = arabicNumber * elementValueConversion;
            return getFormattedResult(element, galacticNumber, result);

        } catch (GalacticException e) {
            return I_DON_T_KNOW_WHAT_YOU_RE_TALKING_ABOUT;
        }

    }


    private String getFormattedResult(String element, String galacticNumber, float result) {
        if (result == (long) result) {
            return String.format(answer, galacticNumber, element, (long) result);
        } else {
            return String.format(answer, galacticNumber, element, result);
        }
    }
}
