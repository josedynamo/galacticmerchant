package com.jose.galaxy.parser;

import com.jose.galaxy.parser.impl.CreditsGalacticParser;
import com.jose.galaxy.parser.impl.ElementValueGalacticParser;
import com.jose.galaxy.parser.impl.NumeralConversionGalacticParser;
import com.jose.galaxy.parser.impl.SimpleQuestionGalacticParser;

/**
 * Factory for the Galactic Parser. Uses RegEx to see if the galactic parser can match that
 * string and returns the corresponding parser.
 */
public class GalacticParserFactory {

    private GalacticParserFactory() {

    }

    public static GalacticParser getGalacticParser(String instruction) {
        if (instruction.matches(NumeralConversionGalacticParser.REG_EX)) {
            return new NumeralConversionGalacticParser();
        } else if (instruction.matches(ElementValueGalacticParser.REG_EX)) {
            return new ElementValueGalacticParser();
        } else if (instruction.matches(SimpleQuestionGalacticParser.REG_EX)) {
            return new SimpleQuestionGalacticParser();
        } else if (instruction.matches(CreditsGalacticParser.REG_EX)) {
            return new CreditsGalacticParser();
        } else return null;
    }
}
