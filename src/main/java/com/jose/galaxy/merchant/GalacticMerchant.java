package com.jose.galaxy.merchant;

import com.jose.galaxy.conversions.GalacticElementConversion;
import com.jose.galaxy.conversions.GalacticNumeralConversion;
import com.jose.galaxy.parser.GalacticParser;
import com.jose.galaxy.parser.GalacticParserFactory;

/**
 * Gets the proper Galactic Parser based on the type of instruction and returns the result of the parsing.
 */
public class GalacticMerchant {

    private final GalacticElementConversion galacticElementConversion;
    private final GalacticNumeralConversion galacticNumeralConversion;

    public GalacticMerchant() {
        galacticNumeralConversion = new GalacticNumeralConversion();
        galacticElementConversion = new GalacticElementConversion();

    }

    public String readInstruction(String galacticInstruction) {
        GalacticParser galacticParser = GalacticParserFactory.getGalacticParser(galacticInstruction);
        if (galacticParser != null) {
            return galacticParser.parse(galacticInstruction, galacticNumeralConversion, galacticElementConversion);
        } else {
            return GalacticParser.I_DON_T_KNOW_WHAT_YOU_RE_TALKING_ABOUT;
        }

    }
}
