package com.jose.galaxy.testmerchant;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import com.jose.galaxy.merchant.GalacticMerchant;

/***
 * Main class used to read a file and parse the results by a Galactic Merchant
 *
 */
public class TestMerchant {

    public static void main(String args[]) {
        if (args.length != 0) {
            System.out.println("Processing galactic file: " + args[0]);
            try {
                File file = new File(args[0]);
                TestMerchant testMerchant = new TestMerchant();
                testMerchant.readFile(file.getCanonicalPath());
            } catch (IOException e) {
                System.out.println("Couldn't read file ");
            }
        }
        else
        {   System.out.println("Please add a filename to the arguments");
            System.out.println("Usage: java merchant-1.0-SNAPSHOT.jar <filename>");


        }

    }

    private void readFile(String fileName) throws IOException {
        GalacticMerchant galacticMerchant = new GalacticMerchant();
        Stream<String> lines = Files.lines(
                Paths.get(fileName), Charset.defaultCharset());
        lines.forEach(line -> parse(line, galacticMerchant));

    }

    private static void parse(String line, GalacticMerchant galacticMerchant) {
        final String answer = galacticMerchant.readInstruction(line);
        if (!answer.isEmpty()) {
            System.out.println(answer);
        }
    }


}
