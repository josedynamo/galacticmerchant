package com.jose.galaxy.romannumerals;

import com.jose.galaxy.conversions.GalacticNumeralConversion;
import com.jose.galaxy.exception.GalacticNumeralException;

public class RomanNumeralConverter {

    public String getRomanFromGalacticNumber(String galacticNumber, GalacticNumeralConversion galacticNumeralConversion) throws GalacticNumeralException {
        final String[] numbers = galacticNumber.split(" ");
        StringBuilder romanNumeral = new StringBuilder();
        for (String number : numbers) {
            romanNumeral.append(galacticNumeralConversion.getConversion(number));
        }
        return romanNumeral.toString();
    }

    public int toArabic(String romanNumeral) throws GalacticNumeralException {
        if (isValidNumeral(romanNumeral)) {
            return getResult(romanNumeral);
        }
        return 0;

    }

    private int getResult(String romanNumeral) throws GalacticNumeralException {
        for (RomansToArabic romansToArabic : RomansToArabic.values()) {
            if (romanNumeral.startsWith(romansToArabic.roman)) {
                return romansToArabic.arabic + toArabic(romanNumeral.replaceFirst(romansToArabic.roman, ""));
            }
        }
        return 0;
    }

    public boolean isValidNumeral(String romanNumeral) throws GalacticNumeralException {
        String romanNumeralRegEx = "(M{0,4})(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})";
        if (romanNumeral.matches(romanNumeralRegEx)) {
            return true;
        } else {
            throw new GalacticNumeralException("Oops!!! That galactic numeral is not valid. I can't convert it.");
        }


    }

    private enum RomansToArabic {
        ONE_THOUSAND(1000, "M"),
        NINE_HUNDRED(900, "CM"),
        FIVE_HUNDRED(500, "D"),
        FOUR_HUNDRED(400, "CD"),
        ONE_HUNDRED(100, "C"),
        NINETY(90, "XC"),
        FIFTY(50, "L"),
        FORTY(40, "XL"),
        TEN(10, "X"),
        NINE(9, "IX"),
        FIVE(5, "V"),
        FOUR(4, "IV"),
        ONE(1, "I");

        private final String roman;
        private final int arabic;

        RomansToArabic(int arabic, String roman) {
            this.roman = roman;
            this.arabic = arabic;
        }
    }
}
