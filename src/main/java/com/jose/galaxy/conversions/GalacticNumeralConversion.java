package com.jose.galaxy.conversions;

import java.util.HashMap;
import java.util.Map;

import com.jose.galaxy.exception.GalacticNumeralException;

/**
 * Wrapper used to Map the Galactic Numbers to a Roman Numeral
 */
public class GalacticNumeralConversion {
    private Map<String, String> galacticToRomanMap = new HashMap<>();

    public void addConversion(Map.Entry<String, String> conversion) {
        galacticToRomanMap.put(conversion.getKey(), conversion.getValue());
    }

    public String getConversion(String key) throws GalacticNumeralException {
        final String romanNumeral = galacticToRomanMap.get(key);
        if (romanNumeral != null) {
            return romanNumeral;
        }
        throw new GalacticNumeralException("Oops! You still have to define the roman numeral mapping for " + key);
    }
}
