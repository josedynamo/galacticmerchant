package com.jose.galaxy.conversions;

import com.jose.galaxy.exception.GalacticElementNotFoundException;

import java.util.HashMap;
import java.util.Map;

/**
 * Wrapper used to Map the Elements to their value in Credits
 */
public class GalacticElementConversion {
    private Map<String, Float> elementValueMap = new HashMap<>();

    public void addElementValueConversion(Map.Entry<String, Float> conversion) {
        elementValueMap.put(conversion.getKey(), conversion.getValue());
    }

    public Float getElementValueConversion(String key) throws GalacticElementNotFoundException {
        final Float creditValuePerUnit = elementValueMap.get(key);
        if (creditValuePerUnit != null) {
            return creditValuePerUnit;
        }
        throw new GalacticElementNotFoundException("Oops! You still have to define the credit value for " + key);
    }
}
